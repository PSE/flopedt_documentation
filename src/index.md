---
home: true
heroImage: https://framalibre.org/sites/default/files/styles/thumbnail/public/leslogos/logo-head-gribou_2.png?itok=DSI9Tbh8
tagline:
actionText: C'est parti →
actionLink: /documentation/bien-debuter/installation
footer: Made by Thomas Gouveia.
---

## Bienvenue sur le wiki de flop!EDT

Vous êtes sur le wiki de flop!EDT, logiciel libre pour une gestion coopérative des emplois du temps.

Vous trouvez [ici](_sidebar) toutes les informations nécessaires pour comprendre le fonctionnement de notre application, pour apprendre comment l'installer et comment l'utiliser. Vous pouvez aussi cliquer sur un des liens du menu à droite ---->

Vous pouvez également :

- consulter [notre site web](https://flopedt.org/) sur lequel vous trouverez nos actualités, les moyens de nous soutenir, et une Foire aux Questions (FAQ).
- rejoindre notre [espace de discussion framateam](https://framateam.org/flopedt) pour nous poser des questions, nous soumettre un problème d'utilisation ou simplement échanger avec la communauté
