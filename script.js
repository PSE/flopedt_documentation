import 'dotenv/config'
import { readFileSync, writeFileSync, existsSync, mkdirSync, copyFileSync, rmdirSync, unlinkSync } from 'fs'
import matter from 'gray-matter'
const util = require('util');
const exec = util.promisify(require('child_process').exec);
import recursive from 'recursive-readdir'
import path from 'path'
import { exit } from 'process'
import moment from 'moment'
import Logger from './utils/Logger'
import slugify from 'slugify';

// Définition du log file pour cette exécution
global.currentLogFile = __dirname + '/logs/' + moment().format('DD-MM-YYYY-h-m-ss') + '.log'

const { WIKI_REPOSITORY, WIKI_BRANCH } = process.env
const staticPath = `/images/`
const configFilePath = `${__dirname}/src/.vuepress/config.js`
const documentationPath = `${__dirname}/src/documentation`
const temporaryPath = `${__dirname}/tmp`
const temporaryStaticPath = `${__dirname}/tmp/images/`
const UPDATE_COMMAND = 'npm install && npm run build'

/**
 * Extrait les titres et leur ordre dans le fichier _sidebar.md
 */
const extractHeads = () => {
    const _sidebarFile = `${documentationPath}/_sidebar.md`
    Logger.info(`Lecture du fichier _sidebar.md pour en extraire la structure des fichiers (${_sidebarFile})`)
    // On récupère le contenu du fichier
    const _sidebar = matter(readFileSync(_sidebarFile))
    // On défini la regex permettant d'extraire
    // le titre. Exemple : 1. [Installation](installation)
    const regex = /^[0-9]{0,3}\. \[[a-zA-Z0-9àé' -\/]{0,}\]\([a-zA-Z0-9àé' -\/]{0,}\)$/;
    const heads = []
    // Pour chaque ligne du fichier, on test la ligne avec la regex,
    // si la ligne match, on crée un objet dans lequel on stocke le titre,
    // le slug et le fichier, ce qui permettra par la suite de mettre en place la configuration
    // de la barre de navigation.
    _sidebar.content.split('\n').forEach((line) => {
        if (line.match(regex)) {
            const l = {}
            const notFormattedSlug = line.match(/\([a-zA-Z0-9àé' -\/]{0,}\)/)[0]
                .replace(/\(/g, '')
                .replace(/\)/g, '')
            // On extrait le titre et on remplace les [ et ] dans ce dernier
            l.titre = line.match(/\[[a-zA-Z0-9àé' -\/]{0,}\]/)[0]
                .replace(/\[/g, '')
                .replace(/\]/g, '')
            // On slugify le slug qui permettra de faire le lien
            l.slug = notFormattedSlug.split('/').map(e => {
                if (!e.includes('.')) return slugify(decodeURI(e), { lower: true })
                return e
            }).join('/')
            // On récupère le dossier parent
            l.parent = decodeURI(notFormattedSlug.split('/')[0])
            // On ajoute notre objet dans notre tableau
            Logger.success(`La ligne "${line}" a été extraite du fichier.`)
            heads.push(l)
        }
    })
    Logger.info('Extraction de la structure terminée.')
    return heads
}

/**
 * Lance la commande permettant de mettre à jour la documentation.
 */
const buildDocs = async () => {
    Logger.info(`Mise à jour de la documentation.`)
    const { error, stdout, stderr } = await exec(UPDATE_COMMAND)
    if (error) Logger.error(error)
    if (stderr) Logger.warning(stderr)
    Logger.success(stdout)
    Logger.info('Mise à jour de la documenation terminée.')
}

/**
 * 
 */
const updateRepo = async () => {
    // Si le dossier existe, on utilise git pull
    if (existsSync(temporaryPath)) {
        Logger.info(`Le dossier (${temporaryPath}) existe. Mise à jour des fichiers.`)
        await exec(`cd tmp && git checkout ${WIKI_BRANCH} && git pull`)
        Logger.success('Mise à jour de la documentation terminée.')
    } else {
        // Sinon on crée le dossier, et on clone le dépôt
        Logger.warning(`Le dossier (${temporaryPath}) n'existe pas. Création du dossier et clonage du dépôt.`)
        mkdirSync(temporaryPath)
        const { stdout, stderr } = await exec(`git clone ${WIKI_REPOSITORY} tmp && cd tmp && git checkout ${WIKI_BRANCH}`)
        Logger.success(`Le dépôt a été cloné dans le dossier ${temporaryPath}`)
    }
}

/**
 * Met à jour le fichier de configuration
 */
const updateSidebarConfiguration = () => {
    unlinkSync(configFilePath)
    const docKey = "/documentation/"
    const heads = extractHeads()

    const sidebar = {
        [docKey]: [],
    }
    const parents = [...new Set(heads.map(e => e.parent))]
    parents.forEach(parent => {
        const elements = heads.filter(e => e.parent === parent)
        sidebar[docKey].push({
            title: parent,
            collapsable: true,
            children: elements.map(e => [`${docKey}${e.slug}`, e.titre]),
        })
    })

    const configFileContent = `
    const { description } = require("../../package");

    module.exports = {
      /**
       * Ref：https://v1.vuepress.vuejs.org/config/#title
       */
      title: "FlOpEDT Documentation",
      /**
       * Ref：https://v1.vuepress.vuejs.org/config/#description
       */
      description: description,
    
      /**
       * Extra tags to be injected to the page HTML '<head>'
       *
       * ref：https://v1.vuepress.vuejs.org/config/#head
       */
      head: [
        ["meta", { name: "theme-color", content: "#3eaf7c" }],
        ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
        [
          "meta",
          { name: "apple-mobile-web-app-status-bar-style", content: "black" },
        ],
      ],
    
      /**
       * Theme configuration, here is the default theme configuration for VuePress.
       *
       * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
       */
      themeConfig: {
        repo: "",
        editLinks: false,
        docsDir: "",
        editLinkText: "",
        nav: [
          {
            text: "Documentation",
            link: "/documentation/bien-debuter/installation",
          },
          {
            text: "Framagit",
            link: "https://framagit.org/flopedt/FlOpEDT",
          },
          {
            text: "FlOpEDT.org",
            link: "https://www.flopedt.org/",
          },
        ],
        sidebar: ${JSON.stringify(sidebar)},
      },
    
      /**
       * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
       */
      plugins: ["@vuepress/plugin-back-to-top", "@vuepress/plugin-medium-zoom"],
    };    
    `

    writeFileSync(configFilePath, configFileContent)
}


const task = async () => {
    await updateRepo()
    Logger.info(`Récupération récursive des fichiers dans le répertoire temporaire. (${temporaryPath})`)
    // Préparer le dossier qui contiendra la documentation
    recursive(temporaryPath, ['.*'], (err, files) => {
        if (err) {
            Logger.error(err)
            exit(1)
        }
        Logger.success(`${files.length} fichiers trouvés.`)
        // Pour chacun des fichiers markdown, on va remplacer le chemin par le chemin de destination,
        // puis on va créer le nouveau fichier en prenant soin de remplacer chaque assets par la nouvelle destination.
        // Pour les fichiers assets, on va les déplacer dans le dossier correspondant pour qu'ils soient pris en compte au moment du build.
        for (const file of files) {
            Logger.info(`Traitement du fichier "${file}"`)
            // On récupère l'extension du fichier.
            // Grâce à cette dernière on va pouvoir appliquer ou non les différents traitements en fonction
            // du type de fichier.
            const extension = path.extname(file)
            // Si l'on est sur un fichier de type markdown
            if (extension === '.md') {
                Logger.info('Fichier markdown (.md) détecté. Mise à jour des données puis réécriture du fichier dans le dossier de destination.')

                // On slugify le chemin du fichier
                let newPath = file
                    .replace(`${temporaryPath}/`, '')
                    .split('/')
                    .map(e => {
                        if (!e.includes('.')) return slugify(e, { lower: true })
                        return e
                    })
                    .join('/')

                // On construit le nouveau chemin du fichier
                const newFilePath = `${documentationPath}/${newPath}`
                // On lit le contenu du fichier de manière synchrone
                const buffer = readFileSync(file)
                // On convertit le buffer en un objet JSON
                const fileContent = matter(buffer)
                // On remplace tous les assets par le nouveau chemin
                const newFileContent = fileContent.content.replace(/images\//g, staticPath)
                // On vérifie que le nouveau dossier existe, si ce n'est pas le cas il faut le créé.
                let newDir = newFilePath.split('/')
                newDir = newDir.slice(0, newDir.length - 1).join('/')
                if (!existsSync(newDir)) mkdirSync(newDir)
                // On écrit le contenu dans le nouveau fichier
                writeFileSync(newFilePath, newFileContent)
            } else if (['.jpg', '.jpeg', '.gif', '.png'].includes(extension)) {
                Logger.info(`Fichier assets (.jpg, .jpeg, .gif, .png) détecté. Copie de ce fichier dans le dossier de destination.`)
                // Si l'on est sur un fichier de type image, on le copie
                copyFileSync(file, `${__dirname}/src/.vuepress/public${file.replace(temporaryStaticPath, staticPath)}`)
            }
        }

        updateSidebarConfiguration()
        // On build maintenant la nouvelle documenation
        buildDocs()
    })

}

task()