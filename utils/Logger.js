import colors from 'colors'
import moment from 'moment'
import { appendFileSync } from 'fs'

/**
 * @class Logger
 * Permet de gérer les affichages dans la console et la création des fichiers de logs
 */
export default class Logger {

    static writeToLogFile(message) {
        const logFile = global.currentLogFile
        const m = `${moment().format('MMM Do YYYY h:mm:ss')} - ${message} \n`
        appendFileSync(logFile, m)
    }

    static separator() {
        console.log('------------------------')
    }

    static success(message) {
        const m = `${'[SUCCESS]'.white.bgGreen.bold} ${message.white.bold}`
        console.log(m)
        Logger.writeToLogFile(message)
        Logger.separator()
    }

    static error(message) {
        const m = `${'[ERROR]'.white.bgRed.bold} ${message.white.bold}`
        console.error(m)
        Logger.writeToLogFile(message)
        Logger.separator()
    }

    static info(message) {
        const m = `${'[NOTICE]'.white.bgBlue.bold} ${message.white.bold}`
        console.info(m)
        Logger.writeToLogFile(message)
        Logger.separator()
    }

    static warning(message) {
        const m = `${'[WARNING]'.white.bgYellow.bold} ${message.white.bold}`
        console.warn(m)
        Logger.writeToLogFile(message)
        Logger.separator()
    }
}